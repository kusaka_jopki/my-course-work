@extends('layouts.app')

@section('content')
    <div class="row-cols-1">
        <div class="row justify-content-center">
            <div class="col-4">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col"><h3>Edit: {{$room->id}}</h3></div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="/admin/rooms/{{$room->id}}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <select name="type" class="form-control">
                                    @foreach($types as $key => $type)
                                        <option value="{{$key}}" {{ ($room->type == $type) ? 'selected' : '' }}> {{$type}} </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row">
                                <input type="text" name="count" class="form-control mt-2" value="{{$room->count}}" placeholder="Сapacity" required>
                            </div>
                            <div class="row">
                                <input type="text" name="cost" class="form-control mt-2" value="{{$room->cost}}" placeholder="Cost" required>
                            </div>
                            <div class="row">
                                <select name="state" class="form-control mt-2">
                                    @foreach($states as $key => $state)
                                        <option value="{{$key}}"{{($room->room_state == $key) ? 'selected' : ''}}>{{$state}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row">
                                <input type="submit" class="btn btn-success form-control mt-2" value="Save">
                            </div>
                            <div class="row justify-content-center">
                                <a href="{{\Illuminate\Support\Facades\URL::previous()}}">Повернутись</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
