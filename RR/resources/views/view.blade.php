@extends('layouts.app')

@section('content')

    <div class="row-cols-1">
        <div class="row justify-content-center">
            <div class="col-10">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col"><h1>Rooms:</h1></div>
                            <div class="col"><a class="btn btn-info float-right" href="/admin/rooms/create">Add room</a></div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card">
                            <div class="card-body">
                                <form action="">
                                    <div class="form-row">
                                        <div class="col">
                                            <select class="form-control" name="type">
                                                <option value="all" {{($request->type == 'all' ? 'selected' : '')}}>All rooms </option>
                                                <option value="0" {{($request->type == '0' ? 'selected' : '')}}>Vip</option>
                                                <option value="1" {{($request->type == '1' ? 'selected' : '')}}>Common</option>
                                                <option value="2" {{($request->type == '2' ? 'selected' : '')}}>LP</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <select class="form-control" name="state">
                                                <option value="all" {{($request->state == 'all' ? 'selected' : '')}}>All states</option>
                                                <option value="0" {{($request->state == '0' ? 'selected' : '')}}>Free</option>
                                                <option value="1" {{($request->state == '1' ? 'selected' : '')}}>Busy</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <input type="submit" class="btn btn-success form-control" value="Show">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <table class="table table-sm">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Room type</th>
                                <th scope="col">Capacity</th>
                                <th scope="col">Cost</th>
                                <th scope="col">State</th>
                                <th scope="col">Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $room)
                                <tr>
                                    <th scope="row"><a href="/admin/rooms/{{$room->id}}">{{$room->id}}</a></th>
                                    <td>{{$room->type}}</td>
                                    <td>{{$room->count}}</td>
                                    <td>{{$room->cost}}</td>
                                    <td class="alert {{$room->room_state == '1' ? 'alert-danger' : 'alert-success'}}">
                                        {{$room->room_state == '1' ? 'BUSY' : 'FREE' }}
                                    </td>
                                    <td>
                                        <a class="btn btn-sm btn-warning btn-block" href="/admin/rooms/{{$room->id}}/edit">Edit</a>
                                        <form action="/admin/rooms/{{$room->id}}" method="post">
                                            @method('DELETE')
                                            @csrf
                                            <input type="submit" class="btn btn-sm btn-danger btn-block" value="Delete">
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
