@extends('layouts.app')

@section('content')
    <div class="row-cols-1">
        <div class="row justify-content-center">
            <div class="col-5">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col"><h3>Add:</h3></div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="/admin/rooms" method="post">
                            @csrf
                            <div class="row">
                                <select name="type">
                                    @foreach($types as $key => $type)
                                        <option value="{{$key}}">{{$type}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row">
                                <input type="text" name="count" placeholder="Capacity" required>
                            </div>
                            <div class="row">
                                <input type="text" name="cost" placeholder="Cost" required>
                            </div>
                            <div class="row">
                                <select name="state">
                                    @foreach($states as $key => $state)
                                        <option value="{{$key}}">{{$state}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row">
                                <input type="submit" value="Save">
                            </div>
                            <div class="row">
                                <a href="{{\Illuminate\Support\Facades\URL::previous()}}">Повернутись</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
