@extends('layouts.app')

@section('content')
    <div class="row-cols-1">
        <div class="row justify-content-center">
            <div class="col-5">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col"><h3>View: {{$room->id}}</h3></div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>ID: {{$room->id}}</p>
                        <p>Type: {{$room->type}}</p>
                        <p>Capacity: {{$room->count}}</p>
                        <p>Price: {{$room->cost}}</p>
                        <p>State: {{$room->room_state == '1' ? 'BUSY' : 'FREE' }}</p>
                        <a class="btn btn-sm btn-warning btn-block" href="/admin/rooms/{{$room->id}}/edit">Edit</a>
                        <form action="/admin/rooms/{{$room->id}}" method="post">
                            @method('DELETE')
                            @csrf
                            <input type="submit" class="btn btn-sm btn-danger btn-block" value="Delete">
                        </form>
                        <hr>
                        <a href="{{\Illuminate\Support\Facades\URL::previous()}}">Return</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
