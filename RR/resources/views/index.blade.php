@extends('layouts.app')

@section('content')
    <div class="row-cols-1">
        <div class="row justify-content-center">
            <div class="col-10">
                <div class="card">
                    <div class="card-body">
                        Головна сторінка
                    </div>
                </div>
                <div class="card mt-2">
                    <div class="card-body">
                        <table class="table table-sm">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Room type</th>
                                <th scope="col">Capacity</th>
                                <th scope="col">Cost</th>
                                <th scope="col">State</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $room)
                                <tr>
                                    <th scope="row"><a href="/admin/rooms/{{$room->id}}">{{$room->id}}</a></th>
                                    <td>{{$room->type}}</td>
                                    <td>{{$room->count}}</td>
                                    <td>{{$room->cost}}</td>
                                    <td class="alert {{$room->room_state == '1' ? 'alert-danger' : 'alert-success'}}">
                                        {{$room->room_state == '1' ? 'BUSY' : 'FREE' }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
