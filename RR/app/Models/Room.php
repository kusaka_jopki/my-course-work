<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;


    public static $types = [
        '0' => 'VIP',
        '1' =>'COMMON',
        '2' =>'LP'
    ];

    public static $states = [
      '0' => 'Free',
      '1' => 'Busy'
    ];

    protected $fillable = [
        'id',
        'type',
        'count',
        'cost',
        'room_state'
    ];

    public $timestamps = false;

}
