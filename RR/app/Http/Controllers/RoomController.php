<?php

namespace App\Http\Controllers;

use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth', ['except' => ['show']]);
    }

    public function index(Request $request)
    {
        $rooms = Room::query();
        if ($request->filled('type')){
            switch ($request->type){
                case '0':
                    $rooms->where('type', 'VIP');
                    break;
                case '1':
                    $rooms->where('type', 'COMMON');
                    break;
                case '2':
                    $rooms->where('type', 'LP');
                    break;
            }
        }
        if ($request->filled('state')){
            switch ($request->state){
                case '0':
                    $rooms->where('room_state', '0');
                    break;
                case '1':
                    $rooms->where('room_state', '1');
                    break;
            }
        }
        return view('view', ['request' => $request, 'data' => $rooms->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('add', ['types' => Room::$types, 'states' => Room::$states]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Room $room)
    {
        $room = new Room();
        $room->type = Room::$types[$request->type];
        $room->count = $request->count;
        $room->cost = $request->cost;
        $room->room_state = $request->state;
        $room->save();
        return Redirect::to('/admin/rooms');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $room = Room::query()->where('id', $id)->first();
        return view('room', ['room' => $room]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function edit($id)
        {
            $room = Room::query()->where('id', $id)->first();
            return view('edit', ['room' => $room, 'types' => Room::$types, 'states' => Room::$states]);
        }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $room = Room::query()->where('id', $id)->first();
        $room->type = Room::$types[$request->type];
        $room->count = $request->count;
        $room->cost = $request->cost;
        $room->room_state = $request->state;
        $room->save();
        return Redirect::to('/admin/rooms');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Room::destroy($id);
        return Redirect::back();
    }
}
