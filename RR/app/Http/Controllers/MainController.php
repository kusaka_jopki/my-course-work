<?php

namespace App\Http\Controllers;

use App\Models\Room;
use Illuminate\Http\Request;

class MainController extends Controller
{
    function index(){
        $data = Room::query()->where('room_state', '0')->get();
        return view('index', ['data' => $data]);
    }

}
