<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\RoomController;
use Illuminate\Support\Facades\Auth;;


Route::get('/', [MainController::class, 'index']);


Auth::routes();

Route::resource('admin/rooms', RoomController::class);
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/admin', [HomeController::class, 'index'])->name('home');
