<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UserTest extends TestCase
{

    public function test_a_index_view_can_be_tested()
    {
        $view = $this->view('index', ['id' => '8']);

        $view->assertSee('8');
    }

}
